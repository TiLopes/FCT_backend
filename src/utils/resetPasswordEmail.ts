import { USER } from "../models/user";

import nodemailer from "nodemailer";
import logger from "./logger";

const resetPasswordEmail = async (user: USER, hash: string) => {
	const transport = nodemailer.createTransport({
		host: "sandbox.smtp.mailtrap.io",
		port: 2525,
		auth: {
			user: process.env.EMAIL_SENDER_DEV,
			pass: process.env.EMAIL_PASS_DEV,
		},
	});

	const message = await transport.sendMail({
		from: {
			address: process.env.EMAIL_SENDER_DEV!,
			name: "FCT PROJETO",
		},
		to: user.email,
		subject: "Alterar password da sua conta",
		html: `
        <p>Recebemos um pedido para alterar a palavra passe da sua contar</p>
        <p>Utilize este link: <a href='http://localhost:4200/password-reset/${hash}'>http://localhost:4200/password-reset/${hash}</a></p>
        `,
	});

	logger.info(message.messageId);
};

export default resetPasswordEmail;

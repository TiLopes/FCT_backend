import { Router } from "express";
import {
	addVenda,
	deleteVenda,
	getVenda,
	getVendas,
} from "../controllers/vendas.controller";
import verifyJWT from "../middleware/verifyJWT";
import verifyPermission from "../middleware/verifyPermissions";

const router = Router();

router.get("/get/vendas", verifyJWT, verifyPermission, getVendas);

router.post("/add/venda", verifyJWT, verifyPermission, addVenda);

router.get("/get/venda/:id", verifyJWT, verifyPermission, getVenda);

// router.post("/update/venda/:id", verifyJWT, verifyPermission, updateVenda);

router.post("/delete/venda/:id", verifyJWT, verifyPermission, deleteVenda);

export default router;

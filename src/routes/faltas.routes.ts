import { Router } from "express";
import {
	addFalta,
	getFaltas,
	justificarFalta,
} from "../controllers/faltas.controller";
import verifyJWT from "../middleware/verifyJWT";
import verifyPermission from "../middleware/verifyPermissions";

const router = Router();

router.get("/get/faltas", verifyJWT, verifyPermission, getFaltas);
router.post(
	"/justificar/falta/:id",
	verifyJWT,
	verifyPermission,
	justificarFalta
);
router.post("/add/falta", verifyJWT, verifyPermission, addFalta);

export default router;

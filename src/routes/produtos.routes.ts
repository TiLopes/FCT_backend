import { Router } from "express";
import {
	addProduto,
	deleteProduto,
	getProduto,
	getProdutos,
	updateProduto,
} from "../controllers/produtos.controller";
import verifyJWT from "../middleware/verifyJWT";
import verifyPermission from "../middleware/verifyPermissions";

const router = Router();

router.get("/get/produtos", verifyJWT, verifyPermission, getProdutos);

router.post("/add/produto", verifyJWT, verifyPermission, addProduto);

router.get("/get/produto/:id", verifyJWT, verifyPermission, getProduto);

router.post("/update/produto/:id", verifyJWT, verifyPermission, updateProduto);

router.post("/delete/produto/:id", verifyJWT, verifyPermission, deleteProduto);

export default router;

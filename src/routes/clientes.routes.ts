import { Router } from "express";
import {
	addCliente,
	getCliente,
	getClientes,
	updateCliente,
} from "../controllers/clientes.controller";
import verifyJWT from "../middleware/verifyJWT";
import verifyPermission from "../middleware/verifyPermissions";

const router = Router();

router.get("/get/clientes", verifyJWT, verifyPermission, getClientes);
router.get("/get/clientes/:type", verifyJWT, verifyPermission, getClientes);

router.post("/add/cliente", verifyJWT, verifyPermission, addCliente);

router.get("/get/cliente/:id", verifyJWT, verifyPermission, getCliente);

router.post("/update/cliente/:id", verifyJWT, verifyPermission, updateCliente);

export default router;

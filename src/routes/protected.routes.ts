import { Router } from "express";
const router = Router();
import {
	personalInfoGET,
	userInfoGET,
} from "../controllers/protected.controller";
import verifyJWT from "../middleware/verifyJWT";
import verifyPermission from "../middleware/verifyPermissions";

router.get(
	"/api/showuser/info/:userId",
	verifyJWT,
	verifyPermission,
	userInfoGET
);

router.get("/api/showuser/me", verifyJWT, verifyPermission, personalInfoGET);

export default router;

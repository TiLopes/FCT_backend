import { Router } from "express";
import { getUsers } from "../controllers/users.controller";
import verifyJWT from "../middleware/verifyJWT";
import verifyPermission from "../middleware/verifyPermissions";

const router = Router();

router.get("/get/users", verifyJWT, verifyPermission, getUsers);
router.get("/get/users/:type", verifyJWT, verifyPermission, getUsers);

export default router;

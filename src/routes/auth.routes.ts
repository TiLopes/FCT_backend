import { Router } from "express";
import jwt, { JwtPayload, VerifyErrors } from "jsonwebtoken";
import {
	loginGET,
	loginPOST,
	resetPassword,
	resetPasswordRequest,
	resetPasswordTokenValid,
	signupGET,
	signupPOST,
} from "../controllers/auth.controller";
import validatePassword from "../middleware/validatePassword";
import validatePasswordLeak from "../middleware/validatePasswordLeak";
import { USER } from "../models/user";
import logger from "../utils/logger";

type DecodedToken = {
	id: number;
	group_id: number;
	iat: number;
	exp: number;
};

const router = Router(); // criar router

router.get("/api/auth/signup", signupGET);
router.post(
	"/api/auth/signup",
	validatePassword,
	validatePasswordLeak,
	signupPOST
);

router.get("/api/auth/login", loginGET);
router.post("/api/auth/login", loginPOST);

router.post("/api/reset/password/request", resetPasswordRequest);
router.get("/api/reset/password/:token", resetPasswordTokenValid);
router.post(
	"/api/reset/password",
	validatePassword,
	validatePasswordLeak,
	resetPassword
);

router.post("/api/verify/auth", (req, res) => {
	const token = req.headers.authorization?.split(" ")[1];

	if (!token) {
		return res.status(401).json({ error: "No token" });
	}

	jwt.verify(
		token,
		process.env.ACCESS_TOKEN_SECRET as string,
		async (
			err: VerifyErrors | null,
			decodedToken: DecodedToken | JwtPayload | string | undefined
		) => {
			if (err) {
				logger.error(err.message);
				return res
					.status(403)
					.json({ success: false, error: "Token is invalid" });
			}
			logger.info({ decodedToken });

			const user = await USER.findOne({ where: { authToken: token } });

			if (!user) {
				return res
					.status(401)
					.json({ success: false, error: "Invalid user token" });
			}

			if (
				typeof decodedToken === "object" &&
				decodedToken.group_id != req.body.group
			) {
				return res.status(401).json({ success: false, error: "Invalid user" });
			}

			req.userID = user.id;
			res.status(200).json({ success: true });
		}
	);
});
// router.get("/api/auth/logout", authController.logout_get);

export default router; // exportar o router

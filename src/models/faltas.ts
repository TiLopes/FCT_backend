import * as Sequelize from "sequelize";
import { DataTypes, Model, Optional } from "sequelize";
import type { USER, USERId } from "./user";

export interface FALTASAttributes {
	id: number;
	id_utilizador: number;
	motivo: string;
	estado: string;
	data_inicio: Date;
	data_fim: Date;
	dia_completo: boolean;
}

export type FALTASPk = "id";
export type FALTASId = FALTAS[FALTASPk];
export type FALTASOptionalAttributes = "id" | "estado" | "dia_completo";
export type FALTASCreationAttributes = Optional<
	FALTASAttributes,
	FALTASOptionalAttributes
>;

export class FALTAS
	extends Model<FALTASAttributes, FALTASCreationAttributes>
	implements FALTASAttributes
{
	id!: number;
	id_utilizador!: number;
	motivo!: string;
	estado!: string;
	data_inicio!: Date;
	data_fim!: Date;
	dia_completo!: boolean;

	// FALTAS belongsTo USER via id_utilizador
	id_utilizador_user!: USER;
	getId_utilizador_user!: Sequelize.BelongsToGetAssociationMixin<USER>;
	setId_utilizador_user!: Sequelize.BelongsToSetAssociationMixin<USER, USERId>;
	createId_utilizador_user!: Sequelize.BelongsToCreateAssociationMixin<USER>;

	static initModel(sequelize: Sequelize.Sequelize): typeof FALTAS {
		return FALTAS.init(
			{
				id: {
					autoIncrement: true,
					type: DataTypes.INTEGER,
					allowNull: false,
					primaryKey: true,
				},
				id_utilizador: {
					type: DataTypes.INTEGER,
					allowNull: false,
					references: {
						model: "user",
						key: "id",
					},
				},
				motivo: {
					type: DataTypes.STRING(255),
					allowNull: false,
				},
				estado: {
					type: DataTypes.STRING(20),
					allowNull: false,
					defaultValue: "Injustificada",
				},
				data_inicio: {
					type: DataTypes.DATE,
					allowNull: false,
				},
				data_fim: {
					type: DataTypes.DATE,
					allowNull: false,
				},
				dia_completo: {
					type: DataTypes.BOOLEAN,
					allowNull: false,
					defaultValue: false,
				},
			},
			{
				sequelize,
				tableName: "faltas",
				timestamps: false,
				indexes: [
					{
						name: "PRIMARY",
						unique: true,
						using: "BTREE",
						fields: [{ name: "id" }],
					},
					{
						name: "id_utilizador",
						using: "BTREE",
						fields: [{ name: "id_utilizador" }],
					},
				],
			}
		);
	}
}

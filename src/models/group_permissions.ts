import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { GROUPS, GROUPSId } from './groups';
import type { PERMISSIONS, PERMISSIONSId } from './permissions';

export interface GROUP_PERMISSIONSAttributes {
  group_id: number;
  permission_id: string;
}

export type GROUP_PERMISSIONSPk = "group_id" | "permission_id";
export type GROUP_PERMISSIONSId = GROUP_PERMISSIONS[GROUP_PERMISSIONSPk];
export type GROUP_PERMISSIONSCreationAttributes = GROUP_PERMISSIONSAttributes;

export class GROUP_PERMISSIONS extends Model<GROUP_PERMISSIONSAttributes, GROUP_PERMISSIONSCreationAttributes> implements GROUP_PERMISSIONSAttributes {
  group_id!: number;
  permission_id!: string;

  // GROUP_PERMISSIONS belongsTo GROUPS via group_id
  group!: GROUPS;
  getGroup!: Sequelize.BelongsToGetAssociationMixin<GROUPS>;
  setGroup!: Sequelize.BelongsToSetAssociationMixin<GROUPS, GROUPSId>;
  createGroup!: Sequelize.BelongsToCreateAssociationMixin<GROUPS>;
  // GROUP_PERMISSIONS belongsTo PERMISSIONS via permission_id
  permission!: PERMISSIONS;
  getPermission!: Sequelize.BelongsToGetAssociationMixin<PERMISSIONS>;
  setPermission!: Sequelize.BelongsToSetAssociationMixin<PERMISSIONS, PERMISSIONSId>;
  createPermission!: Sequelize.BelongsToCreateAssociationMixin<PERMISSIONS>;

  static initModel(sequelize: Sequelize.Sequelize): typeof GROUP_PERMISSIONS {
    return GROUP_PERMISSIONS.init({
    group_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'groups',
        key: 'id'
      }
    },
    permission_id: {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'permissions',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'group_permissions',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "group_id" },
          { name: "permission_id" },
        ]
      },
      {
        name: "groupPerm_FK_idx",
        using: "BTREE",
        fields: [
          { name: "group_id" },
        ]
      },
      {
        name: "permission_FK_idx",
        using: "BTREE",
        fields: [
          { name: "permission_id" },
        ]
      },
    ]
  });
  }
}

import type { Sequelize } from "sequelize";
import type {
	CLIENTESAttributes,
	CLIENTESCreationAttributes,
} from "./clientes";
import { CLIENTES as _CLIENTES } from "./clientes";
import type { FALTASAttributes, FALTASCreationAttributes } from "./faltas";
import { FALTAS as _FALTAS } from "./faltas";
import type {
	GROUP_PERMISSIONSAttributes,
	GROUP_PERMISSIONSCreationAttributes,
} from "./group_permissions";
import { GROUP_PERMISSIONS as _GROUP_PERMISSIONS } from "./group_permissions";
import type { GROUPSAttributes, GROUPSCreationAttributes } from "./groups";
import { GROUPS as _GROUPS } from "./groups";
import type {
	PERMISSIONSAttributes,
	PERMISSIONSCreationAttributes,
} from "./permissions";
import { PERMISSIONS as _PERMISSIONS } from "./permissions";
import type {
	PRODUTOSAttributes,
	PRODUTOSCreationAttributes,
} from "./produtos";
import { PRODUTOS as _PRODUTOS } from "./produtos";
import type { USERAttributes, USERCreationAttributes } from "./user";
import { USER as _USER } from "./user";
import type { VENDASAttributes, VENDASCreationAttributes } from "./vendas";
import { VENDAS as _VENDAS } from "./vendas";
import type {
	VENDAS_PRODUTOSAttributes,
	VENDAS_PRODUTOSCreationAttributes,
} from "./vendas_produtos";
import { VENDAS_PRODUTOS as _VENDAS_PRODUTOS } from "./vendas_produtos";

export {
	_CLIENTES as CLIENTES,
	_FALTAS as FALTAS,
	_GROUP_PERMISSIONS as GROUP_PERMISSIONS,
	_GROUPS as GROUPS,
	_PERMISSIONS as PERMISSIONS,
	_PRODUTOS as PRODUTOS,
	_USER as USER,
	_VENDAS as VENDAS,
	_VENDAS_PRODUTOS as VENDAS_PRODUTOS,
};
export type {
	CLIENTESAttributes,
	CLIENTESCreationAttributes,
	FALTASAttributes,
	FALTASCreationAttributes,
	GROUP_PERMISSIONSAttributes,
	GROUP_PERMISSIONSCreationAttributes,
	GROUPSAttributes,
	GROUPSCreationAttributes,
	PERMISSIONSAttributes,
	PERMISSIONSCreationAttributes,
	PRODUTOSAttributes,
	PRODUTOSCreationAttributes,
	USERAttributes,
	USERCreationAttributes,
	VENDASAttributes,
	VENDASCreationAttributes,
	VENDAS_PRODUTOSAttributes,
	VENDAS_PRODUTOSCreationAttributes,
};

export function initModels(sequelize: Sequelize) {
	const CLIENTES = _CLIENTES.initModel(sequelize);
	const FALTAS = _FALTAS.initModel(sequelize);
	const GROUP_PERMISSIONS = _GROUP_PERMISSIONS.initModel(sequelize);
	const GROUPS = _GROUPS.initModel(sequelize);
	const PERMISSIONS = _PERMISSIONS.initModel(sequelize);
	const PRODUTOS = _PRODUTOS.initModel(sequelize);
	const USER = _USER.initModel(sequelize);
	const VENDAS = _VENDAS.initModel(sequelize);
	const VENDAS_PRODUTOS = _VENDAS_PRODUTOS.initModel(sequelize);

	GROUPS.belongsToMany(PERMISSIONS, {
		as: "permission_id_permissions",
		through: GROUP_PERMISSIONS,
		foreignKey: "group_id",
		otherKey: "permission_id",
	});
	PERMISSIONS.belongsToMany(GROUPS, {
		as: "group_id_groups",
		through: GROUP_PERMISSIONS,
		foreignKey: "permission_id",
		otherKey: "group_id",
	});
	PRODUTOS.belongsToMany(VENDAS, {
		as: "id_venda_vendas",
		through: VENDAS_PRODUTOS,
		foreignKey: "id_produto",
		otherKey: "id_venda",
	});
	VENDAS.belongsToMany(PRODUTOS, {
		as: "id_produto_produtos",
		through: VENDAS_PRODUTOS,
		foreignKey: "id_venda",
		otherKey: "id_produto",
	});
	VENDAS.belongsTo(CLIENTES, {
		as: "id_cliente_cliente",
		foreignKey: "id_cliente",
	});
	CLIENTES.hasMany(VENDAS, { as: "vendas", foreignKey: "id_cliente" });
	GROUP_PERMISSIONS.belongsTo(GROUPS, { as: "group", foreignKey: "group_id" });
	GROUPS.hasMany(GROUP_PERMISSIONS, {
		as: "group_permissions",
		foreignKey: "group_id",
	});
	USER.belongsTo(GROUPS, { as: "group", foreignKey: "group_id" });
	GROUPS.hasMany(USER, { as: "users", foreignKey: "group_id" });
	GROUP_PERMISSIONS.belongsTo(PERMISSIONS, {
		as: "permission",
		foreignKey: "permission_id",
	});
	PERMISSIONS.hasMany(GROUP_PERMISSIONS, {
		as: "group_permissions",
		foreignKey: "permission_id",
	});
	VENDAS_PRODUTOS.belongsTo(PRODUTOS, {
		as: "id_produto_produto",
		foreignKey: "id_produto",
	});
	PRODUTOS.hasMany(VENDAS_PRODUTOS, {
		as: "vendas_produtos",
		foreignKey: "id_produto",
	});
	FALTAS.belongsTo(USER, {
		as: "id_utilizador_user",
		foreignKey: "id_utilizador",
	});
	USER.hasMany(FALTAS, { as: "falta", foreignKey: "id_utilizador" });
	VENDAS_PRODUTOS.belongsTo(VENDAS, {
		as: "id_venda_venda",
		foreignKey: "id_venda",
	});
	VENDAS.hasMany(VENDAS_PRODUTOS, {
		as: "vendas_produtos",
		foreignKey: "id_venda",
	});

	return {
		CLIENTES: CLIENTES,
		FALTAS: FALTAS,
		GROUP_PERMISSIONS: GROUP_PERMISSIONS,
		GROUPS: GROUPS,
		PERMISSIONS: PERMISSIONS,
		PRODUTOS: PRODUTOS,
		USER: USER,
		VENDAS: VENDAS,
		VENDAS_PRODUTOS: VENDAS_PRODUTOS,
	};
}

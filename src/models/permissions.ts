import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { GROUP_PERMISSIONS, GROUP_PERMISSIONSId } from './group_permissions';
import type { GROUPS, GROUPSId } from './groups';

export interface PERMISSIONSAttributes {
  id: string;
  description: string;
}

export type PERMISSIONSPk = "id";
export type PERMISSIONSId = PERMISSIONS[PERMISSIONSPk];
export type PERMISSIONSCreationAttributes = PERMISSIONSAttributes;

export class PERMISSIONS extends Model<PERMISSIONSAttributes, PERMISSIONSCreationAttributes> implements PERMISSIONSAttributes {
  id!: string;
  description!: string;

  // PERMISSIONS hasMany GROUP_PERMISSIONS via permission_id
  group_permissions!: GROUP_PERMISSIONS[];
  getGroup_permissions!: Sequelize.HasManyGetAssociationsMixin<GROUP_PERMISSIONS>;
  setGroup_permissions!: Sequelize.HasManySetAssociationsMixin<GROUP_PERMISSIONS, GROUP_PERMISSIONSId>;
  addGroup_permission!: Sequelize.HasManyAddAssociationMixin<GROUP_PERMISSIONS, GROUP_PERMISSIONSId>;
  addGroup_permissions!: Sequelize.HasManyAddAssociationsMixin<GROUP_PERMISSIONS, GROUP_PERMISSIONSId>;
  createGroup_permission!: Sequelize.HasManyCreateAssociationMixin<GROUP_PERMISSIONS>;
  removeGroup_permission!: Sequelize.HasManyRemoveAssociationMixin<GROUP_PERMISSIONS, GROUP_PERMISSIONSId>;
  removeGroup_permissions!: Sequelize.HasManyRemoveAssociationsMixin<GROUP_PERMISSIONS, GROUP_PERMISSIONSId>;
  hasGroup_permission!: Sequelize.HasManyHasAssociationMixin<GROUP_PERMISSIONS, GROUP_PERMISSIONSId>;
  hasGroup_permissions!: Sequelize.HasManyHasAssociationsMixin<GROUP_PERMISSIONS, GROUP_PERMISSIONSId>;
  countGroup_permissions!: Sequelize.HasManyCountAssociationsMixin;
  // PERMISSIONS belongsToMany GROUPS via permission_id and group_id
  group_id_groups!: GROUPS[];
  getGroup_id_groups!: Sequelize.BelongsToManyGetAssociationsMixin<GROUPS>;
  setGroup_id_groups!: Sequelize.BelongsToManySetAssociationsMixin<GROUPS, GROUPSId>;
  addGroup_id_group!: Sequelize.BelongsToManyAddAssociationMixin<GROUPS, GROUPSId>;
  addGroup_id_groups!: Sequelize.BelongsToManyAddAssociationsMixin<GROUPS, GROUPSId>;
  createGroup_id_group!: Sequelize.BelongsToManyCreateAssociationMixin<GROUPS>;
  removeGroup_id_group!: Sequelize.BelongsToManyRemoveAssociationMixin<GROUPS, GROUPSId>;
  removeGroup_id_groups!: Sequelize.BelongsToManyRemoveAssociationsMixin<GROUPS, GROUPSId>;
  hasGroup_id_group!: Sequelize.BelongsToManyHasAssociationMixin<GROUPS, GROUPSId>;
  hasGroup_id_groups!: Sequelize.BelongsToManyHasAssociationsMixin<GROUPS, GROUPSId>;
  countGroup_id_groups!: Sequelize.BelongsToManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof PERMISSIONS {
    return PERMISSIONS.init({
    id: {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true
    },
    description: {
      type: DataTypes.STRING(45),
      allowNull: false,
      unique: "description_UNIQUE"
    }
  }, {
    sequelize,
    tableName: 'permissions',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "description_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "description" },
        ]
      },
    ]
  });
  }
}

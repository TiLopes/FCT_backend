import { Sequelize } from "sequelize";
import logger from "../utils/logger";

const db: Sequelize = new Sequelize({
	username: "root",
	password: "root",
	database: "app",
	host: "mysqldb",
	dialect: "mysql",
	logging: (sql, timing) =>
		logger.info(
			sql,
			typeof timing === "number" ? `Elapsed time: ${timing}ms` : ""
		),
	// logging: false
});

export default db;

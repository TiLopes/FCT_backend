import bcrypt from "bcrypt";
import * as Sequelize from "sequelize";
import { DataTypes, Model, Optional } from "sequelize";
import type { FALTAS, FALTASId } from "./faltas";
import type { GROUPS, GROUPSId } from "./groups";

export interface USERAttributes {
	id: number;
	nome: string;
	nif: number;
	cod_postal: string;
	morada: string;
	email: string;
	password: string;
	group_id: number;
	createdAt?: string;
	updatedAt?: string;
	authToken?: string;
	recover_password_token?: string;
	recover_password_token_date?: string;
}

export type USERPk = "id";
export type USERId = USER[USERPk];
export type USEROptionalAttributes =
	| "id"
	| "group_id"
	| "createdAt"
	| "updatedAt"
	| "authToken"
	| "recover_password_token"
	| "recover_password_token_date";
export type USERCreationAttributes = Optional<
	USERAttributes,
	USEROptionalAttributes
>;

export class USER
	extends Model<USERAttributes, USERCreationAttributes>
	implements USERAttributes
{
	id!: number;
	nome!: string;
	nif!: number;
	cod_postal!: string;
	morada!: string;
	email!: string;
	password!: string;
	group_id!: number;
	createdAt?: string;
	updatedAt?: string;
	authToken?: string;
	recover_password_token?: string;
	recover_password_token_date?: string;

	// USER belongsTo GROUPS via group_id
	group!: GROUPS;
	getGroup!: Sequelize.BelongsToGetAssociationMixin<GROUPS>;
	setGroup!: Sequelize.BelongsToSetAssociationMixin<GROUPS, GROUPSId>;
	createGroup!: Sequelize.BelongsToCreateAssociationMixin<GROUPS>;
	// USER hasMany FALTAS via id_utilizador
	falta!: FALTAS[];
	getFalta!: Sequelize.HasManyGetAssociationsMixin<FALTAS>;
	setFalta!: Sequelize.HasManySetAssociationsMixin<FALTAS, FALTASId>;
	addFaltum!: Sequelize.HasManyAddAssociationMixin<FALTAS, FALTASId>;
	addFalta!: Sequelize.HasManyAddAssociationsMixin<FALTAS, FALTASId>;
	createFaltum!: Sequelize.HasManyCreateAssociationMixin<FALTAS>;
	removeFaltum!: Sequelize.HasManyRemoveAssociationMixin<FALTAS, FALTASId>;
	removeFalta!: Sequelize.HasManyRemoveAssociationsMixin<FALTAS, FALTASId>;
	hasFaltum!: Sequelize.HasManyHasAssociationMixin<FALTAS, FALTASId>;
	hasFalta!: Sequelize.HasManyHasAssociationsMixin<FALTAS, FALTASId>;
	countFalta!: Sequelize.HasManyCountAssociationsMixin;

	static initModel(sequelize: Sequelize.Sequelize): typeof USER {
		return USER.init(
			{
				id: {
					autoIncrement: true,
					type: DataTypes.INTEGER,
					allowNull: false,
					primaryKey: true,
				},
				nome: {
					type: DataTypes.STRING(45),
					allowNull: false,
				},
				nif: {
					type: DataTypes.INTEGER,
					allowNull: false,
				},
				cod_postal: {
					type: DataTypes.STRING(8),
					allowNull: false,
				},
				morada: {
					type: DataTypes.STRING(45),
					allowNull: false,
				},
				email: {
					type: DataTypes.STRING(100),
					allowNull: false,
					unique: "email_UNIQUE",
				},
				password: {
					type: DataTypes.STRING(200),
					allowNull: false,
				},
				group_id: {
					type: DataTypes.INTEGER,
					allowNull: false,
					defaultValue: 1,
					references: {
						model: "groups",
						key: "id",
					},
				},
				authToken: {
					type: DataTypes.STRING(320),
					allowNull: true,
					unique: "authToken_UNIQUE",
				},
				recover_password_token: {
					type: DataTypes.STRING(200),
					allowNull: true,
					unique: "recover_password_token_UNIQUE",
				},
				recover_password_token_date: {
					type: "TIMESTAMP",
					allowNull: true,
				},
			},
			{
				hooks: {
					beforeCreate: (user) => {
						const salt = bcrypt.genSaltSync();
						const hash = bcrypt.hashSync(user.getDataValue("password"), salt);
						user.setDataValue("password", hash);
					},
				},
				sequelize,
				tableName: "user",
				timestamps: true,
				indexes: [
					{
						name: "PRIMARY",
						unique: true,
						using: "BTREE",
						fields: [{ name: "id" }],
					},
					{
						name: "email_UNIQUE",
						unique: true,
						using: "BTREE",
						fields: [{ name: "email" }],
					},
					{
						name: "_UNIQUE",
						unique: true,
						using: "BTREE",
						fields: [{ name: "email" }],
					},
					{
						name: "authToken_UNIQUE",
						unique: true,
						using: "BTREE",
						fields: [{ name: "authToken" }],
					},
					{
						name: "recover_password_token_UNIQUE",
						unique: true,
						using: "BTREE",
						fields: [{ name: "recover_password_token" }],
					},
					{
						name: "group_FK_idx",
						using: "BTREE",
						fields: [{ name: "group_id" }],
					},
				],
			}
		);
	}

	static async login(email: string, password: string) {
		if (!email) {
			throw new Error("Email vazio");
		}

		if (!password) {
			throw new Error("Password vazia");
		}

		const user = await USER.findOne({
			where: { email: email },
		});

		if (!user) {
			throw new Error("Esse email não está registado");
		}

		const auth = bcrypt.compareSync(password, user.password);
		if (auth) {
			return user;
		}
		throw new Error("Password errada");
	}

	static async saveToken(id: number, token: string) {
		await USER.update({ authToken: token }, { where: { id } });
	}

	static async removeToken(user: USER) {
		await USER.update({ authToken: "" }, { where: { id: user.id } });
		console.log("Apagado com sucesso");
	}
}

import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { VENDAS, VENDASId } from './vendas';

export interface CLIENTESAttributes {
  id: number;
  nome: string;
  email: string;
  telefone: string;
  nif: number;
  cod_postal: string;
  morada: string;
}

export type CLIENTESPk = "id";
export type CLIENTESId = CLIENTES[CLIENTESPk];
export type CLIENTESOptionalAttributes = "id";
export type CLIENTESCreationAttributes = Optional<CLIENTESAttributes, CLIENTESOptionalAttributes>;

export class CLIENTES extends Model<CLIENTESAttributes, CLIENTESCreationAttributes> implements CLIENTESAttributes {
  id!: number;
  nome!: string;
  email!: string;
  telefone!: string;
  nif!: number;
  cod_postal!: string;
  morada!: string;

  // CLIENTES hasMany VENDAS via id_cliente
  vendas!: VENDAS[];
  getVendas!: Sequelize.HasManyGetAssociationsMixin<VENDAS>;
  setVendas!: Sequelize.HasManySetAssociationsMixin<VENDAS, VENDASId>;
  addVenda!: Sequelize.HasManyAddAssociationMixin<VENDAS, VENDASId>;
  addVendas!: Sequelize.HasManyAddAssociationsMixin<VENDAS, VENDASId>;
  createVenda!: Sequelize.HasManyCreateAssociationMixin<VENDAS>;
  removeVenda!: Sequelize.HasManyRemoveAssociationMixin<VENDAS, VENDASId>;
  removeVendas!: Sequelize.HasManyRemoveAssociationsMixin<VENDAS, VENDASId>;
  hasVenda!: Sequelize.HasManyHasAssociationMixin<VENDAS, VENDASId>;
  hasVendas!: Sequelize.HasManyHasAssociationsMixin<VENDAS, VENDASId>;
  countVendas!: Sequelize.HasManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof CLIENTES {
    return CLIENTES.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nome: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(80),
      allowNull: false,
      unique: "email_UNIQUE"
    },
    telefone: {
      type: DataTypes.STRING(13),
      allowNull: false,
      unique: "telefone_UNIQUE"
    },
    nif: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: "nif_UNIQUE"
    },
    cod_postal: {
      type: DataTypes.STRING(8),
      allowNull: false
    },
    morada: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'clientes',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "email_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "email" },
        ]
      },
      {
        name: "telefone_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "telefone" },
        ]
      },
      {
        name: "nif_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "nif" },
        ]
      },
    ]
  });
  }
}

import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { PRODUTOS, PRODUTOSId } from './produtos';
import type { VENDAS, VENDASId } from './vendas';

export interface VENDAS_PRODUTOSAttributes {
  id_venda: number;
  id_produto: number;
  quantidade: number;
}

export type VENDAS_PRODUTOSPk = "id_venda" | "id_produto";
export type VENDAS_PRODUTOSId = VENDAS_PRODUTOS[VENDAS_PRODUTOSPk];
export type VENDAS_PRODUTOSCreationAttributes = VENDAS_PRODUTOSAttributes;

export class VENDAS_PRODUTOS extends Model<VENDAS_PRODUTOSAttributes, VENDAS_PRODUTOSCreationAttributes> implements VENDAS_PRODUTOSAttributes {
  id_venda!: number;
  id_produto!: number;
  quantidade!: number;

  // VENDAS_PRODUTOS belongsTo PRODUTOS via id_produto
  id_produto_produto!: PRODUTOS;
  getId_produto_produto!: Sequelize.BelongsToGetAssociationMixin<PRODUTOS>;
  setId_produto_produto!: Sequelize.BelongsToSetAssociationMixin<PRODUTOS, PRODUTOSId>;
  createId_produto_produto!: Sequelize.BelongsToCreateAssociationMixin<PRODUTOS>;
  // VENDAS_PRODUTOS belongsTo VENDAS via id_venda
  id_venda_venda!: VENDAS;
  getId_venda_venda!: Sequelize.BelongsToGetAssociationMixin<VENDAS>;
  setId_venda_venda!: Sequelize.BelongsToSetAssociationMixin<VENDAS, VENDASId>;
  createId_venda_venda!: Sequelize.BelongsToCreateAssociationMixin<VENDAS>;

  static initModel(sequelize: Sequelize.Sequelize): typeof VENDAS_PRODUTOS {
    return VENDAS_PRODUTOS.init({
    id_venda: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'vendas',
        key: 'id'
      }
    },
    id_produto: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'produtos',
        key: 'id'
      }
    },
    quantidade: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'vendas_produtos',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id_venda" },
          { name: "id_produto" },
        ]
      },
      {
        name: "vendas_produtos_id_produtos_idx",
        using: "BTREE",
        fields: [
          { name: "id_produto" },
        ]
      },
    ]
  });
  }
}

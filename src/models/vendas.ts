import * as Sequelize from "sequelize";
import { DataTypes, Model, Optional } from "sequelize";
import type { CLIENTES, CLIENTESId } from "./clientes";
import type { PRODUTOS, PRODUTOSId } from "./produtos";
import type { VENDAS_PRODUTOS, VENDAS_PRODUTOSId } from "./vendas_produtos";

export interface VENDASAttributes {
	id: number;
	data_venda: Date;
	id_cliente: number;
	preco: number;
}

export type VENDASPk = "id";
export type VENDASId = VENDAS[VENDASPk];
export type VENDASOptionalAttributes = "id";
export type VENDASCreationAttributes = Optional<
	VENDASAttributes,
	VENDASOptionalAttributes
>;

export class VENDAS
	extends Model<VENDASAttributes, VENDASCreationAttributes>
	implements VENDASAttributes
{
	id!: number;
	data_venda!: Date;
	id_cliente!: number;
	preco!: number;

	// VENDAS belongsTo CLIENTES via id_cliente
	id_cliente_cliente!: CLIENTES;
	getId_cliente_cliente!: Sequelize.BelongsToGetAssociationMixin<CLIENTES>;
	setId_cliente_cliente!: Sequelize.BelongsToSetAssociationMixin<
		CLIENTES,
		CLIENTESId
	>;
	createId_cliente_cliente!: Sequelize.BelongsToCreateAssociationMixin<CLIENTES>;
	// VENDAS belongsToMany PRODUTOS via id_venda and id_produto
	id_produto_produtos!: PRODUTOS[];
	getId_produto_produtos!: Sequelize.BelongsToManyGetAssociationsMixin<PRODUTOS>;
	setId_produto_produtos!: Sequelize.BelongsToManySetAssociationsMixin<
		PRODUTOS,
		PRODUTOSId
	>;
	addId_produto_produto!: Sequelize.BelongsToManyAddAssociationMixin<
		PRODUTOS,
		PRODUTOSId
	>;
	addId_produto_produtos!: Sequelize.BelongsToManyAddAssociationsMixin<
		PRODUTOS,
		PRODUTOSId
	>;
	createId_produto_produto!: Sequelize.BelongsToManyCreateAssociationMixin<PRODUTOS>;
	removeId_produto_produto!: Sequelize.BelongsToManyRemoveAssociationMixin<
		PRODUTOS,
		PRODUTOSId
	>;
	removeId_produto_produtos!: Sequelize.BelongsToManyRemoveAssociationsMixin<
		PRODUTOS,
		PRODUTOSId
	>;
	hasId_produto_produto!: Sequelize.BelongsToManyHasAssociationMixin<
		PRODUTOS,
		PRODUTOSId
	>;
	hasId_produto_produtos!: Sequelize.BelongsToManyHasAssociationsMixin<
		PRODUTOS,
		PRODUTOSId
	>;
	countId_produto_produtos!: Sequelize.BelongsToManyCountAssociationsMixin;
	// VENDAS hasMany VENDAS_PRODUTOS via id_venda
	vendas_produtos!: VENDAS_PRODUTOS[];
	getVendas_produtos!: Sequelize.HasManyGetAssociationsMixin<VENDAS_PRODUTOS>;
	setVendas_produtos!: Sequelize.HasManySetAssociationsMixin<
		VENDAS_PRODUTOS,
		VENDAS_PRODUTOSId
	>;
	addVendas_produto!: Sequelize.HasManyAddAssociationMixin<
		VENDAS_PRODUTOS,
		VENDAS_PRODUTOSId
	>;
	addVendas_produtos!: Sequelize.HasManyAddAssociationsMixin<
		VENDAS_PRODUTOS,
		VENDAS_PRODUTOSId
	>;
	createVendas_produto!: Sequelize.HasManyCreateAssociationMixin<VENDAS_PRODUTOS>;
	removeVendas_produto!: Sequelize.HasManyRemoveAssociationMixin<
		VENDAS_PRODUTOS,
		VENDAS_PRODUTOSId
	>;
	removeVendas_produtos!: Sequelize.HasManyRemoveAssociationsMixin<
		VENDAS_PRODUTOS,
		VENDAS_PRODUTOSId
	>;
	hasVendas_produto!: Sequelize.HasManyHasAssociationMixin<
		VENDAS_PRODUTOS,
		VENDAS_PRODUTOSId
	>;
	hasVendas_produtos!: Sequelize.HasManyHasAssociationsMixin<
		VENDAS_PRODUTOS,
		VENDAS_PRODUTOSId
	>;
	countVendas_produtos!: Sequelize.HasManyCountAssociationsMixin;

	static initModel(sequelize: Sequelize.Sequelize): typeof VENDAS {
		return VENDAS.init(
			{
				id: {
					autoIncrement: true,
					type: DataTypes.INTEGER,
					allowNull: false,
					primaryKey: true,
				},
				data_venda: {
					type: DataTypes.DATE,
					allowNull: false,
				},
				id_cliente: {
					type: DataTypes.INTEGER,
					allowNull: false,
					references: {
						model: "clientes",
						key: "id",
					},
				},
				preco: {
					type: DataTypes.DECIMAL(10, 2),
					allowNull: false,
				},
			},
			{
				sequelize,
				tableName: "vendas",
				timestamps: false,
				indexes: [
					{
						name: "PRIMARY",
						unique: true,
						using: "BTREE",
						fields: [{ name: "id" }],
					},
					{
						name: "venda_cliente_idx",
						using: "BTREE",
						fields: [{ name: "id_cliente" }],
					},
				],
			}
		);
	}
}

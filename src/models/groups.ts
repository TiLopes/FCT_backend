import * as Sequelize from "sequelize";
import { DataTypes, Model } from "sequelize";
import type {
	GROUP_PERMISSIONS,
	GROUP_PERMISSIONSId,
} from "./group_permissions";
import type { PERMISSIONS, PERMISSIONSId } from "./permissions";
import type { USER, USERId } from "./user";

export interface GROUPSAttributes {
	id: number;
	nome: string;
}

export type GROUPSPk = "id";
export type GROUPSId = GROUPS[GROUPSPk];
export type GROUPSCreationAttributes = GROUPSAttributes;

export class GROUPS
	extends Model<GROUPSAttributes, GROUPSCreationAttributes>
	implements GROUPSAttributes
{
	id!: number;
	nome!: string;

	// GROUPS hasMany GROUP_PERMISSIONS via group_id
	group_permissions!: GROUP_PERMISSIONS[];
	getGroup_permissions!: Sequelize.HasManyGetAssociationsMixin<GROUP_PERMISSIONS>;
	setGroup_permissions!: Sequelize.HasManySetAssociationsMixin<
		GROUP_PERMISSIONS,
		GROUP_PERMISSIONSId
	>;
	addGroup_permission!: Sequelize.HasManyAddAssociationMixin<
		GROUP_PERMISSIONS,
		GROUP_PERMISSIONSId
	>;
	addGroup_permissions!: Sequelize.HasManyAddAssociationsMixin<
		GROUP_PERMISSIONS,
		GROUP_PERMISSIONSId
	>;
	createGroup_permission!: Sequelize.HasManyCreateAssociationMixin<GROUP_PERMISSIONS>;
	removeGroup_permission!: Sequelize.HasManyRemoveAssociationMixin<
		GROUP_PERMISSIONS,
		GROUP_PERMISSIONSId
	>;
	removeGroup_permissions!: Sequelize.HasManyRemoveAssociationsMixin<
		GROUP_PERMISSIONS,
		GROUP_PERMISSIONSId
	>;
	hasGroup_permission!: Sequelize.HasManyHasAssociationMixin<
		GROUP_PERMISSIONS,
		GROUP_PERMISSIONSId
	>;
	hasGroup_permissions!: Sequelize.HasManyHasAssociationsMixin<
		GROUP_PERMISSIONS,
		GROUP_PERMISSIONSId
	>;
	countGroup_permissions!: Sequelize.HasManyCountAssociationsMixin;
	// GROUPS belongsToMany PERMISSIONS via group_id and permission_id
	permission_id_permissions!: PERMISSIONS[];
	getPermission_id_permissions!: Sequelize.BelongsToManyGetAssociationsMixin<PERMISSIONS>;
	setPermission_id_permissions!: Sequelize.BelongsToManySetAssociationsMixin<
		PERMISSIONS,
		PERMISSIONSId
	>;
	addPermission_id_permission!: Sequelize.BelongsToManyAddAssociationMixin<
		PERMISSIONS,
		PERMISSIONSId
	>;
	addPermission_id_permissions!: Sequelize.BelongsToManyAddAssociationsMixin<
		PERMISSIONS,
		PERMISSIONSId
	>;
	createPermission_id_permission!: Sequelize.BelongsToManyCreateAssociationMixin<PERMISSIONS>;
	removePermission_id_permission!: Sequelize.BelongsToManyRemoveAssociationMixin<
		PERMISSIONS,
		PERMISSIONSId
	>;
	removePermission_id_permissions!: Sequelize.BelongsToManyRemoveAssociationsMixin<
		PERMISSIONS,
		PERMISSIONSId
	>;
	hasPermission_id_permission!: Sequelize.BelongsToManyHasAssociationMixin<
		PERMISSIONS,
		PERMISSIONSId
	>;
	hasPermission_id_permissions!: Sequelize.BelongsToManyHasAssociationsMixin<
		PERMISSIONS,
		PERMISSIONSId
	>;
	countPermission_id_permissions!: Sequelize.BelongsToManyCountAssociationsMixin;
	// GROUPS hasMany USER via group_id
	users!: USER[];
	getUsers!: Sequelize.HasManyGetAssociationsMixin<USER>;
	setUsers!: Sequelize.HasManySetAssociationsMixin<USER, USERId>;
	addUser!: Sequelize.HasManyAddAssociationMixin<USER, USERId>;
	addUsers!: Sequelize.HasManyAddAssociationsMixin<USER, USERId>;
	createUser!: Sequelize.HasManyCreateAssociationMixin<USER>;
	removeUser!: Sequelize.HasManyRemoveAssociationMixin<USER, USERId>;
	removeUsers!: Sequelize.HasManyRemoveAssociationsMixin<USER, USERId>;
	hasUser!: Sequelize.HasManyHasAssociationMixin<USER, USERId>;
	hasUsers!: Sequelize.HasManyHasAssociationsMixin<USER, USERId>;
	countUsers!: Sequelize.HasManyCountAssociationsMixin;

	static initModel(sequelize: Sequelize.Sequelize): typeof GROUPS {
		return GROUPS.init(
			{
				id: {
					type: DataTypes.INTEGER,
					allowNull: false,
					primaryKey: true,
				},
				nome: {
					type: DataTypes.STRING(45),
					allowNull: false,
					unique: "nome_UNIQUE",
				},
			},
			{
				sequelize,
				tableName: "groups",
				timestamps: false,
				indexes: [
					{
						name: "PRIMARY",
						unique: true,
						using: "BTREE",
						fields: [{ name: "id" }],
					},
					{
						name: "nome_UNIQUE",
						unique: true,
						using: "BTREE",
						fields: [{ name: "nome" }],
					},
				],
			}
		);
	}
}

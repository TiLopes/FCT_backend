import * as Sequelize from "sequelize";
import { DataTypes, Model, Optional } from "sequelize";
import type { VENDAS, VENDASId } from "./vendas";
import type { VENDAS_PRODUTOS, VENDAS_PRODUTOSId } from "./vendas_produtos";

export interface PRODUTOSAttributes {
	id: number;
	nome: string;
	preco: number;
}

export type PRODUTOSPk = "id";
export type PRODUTOSId = PRODUTOS[PRODUTOSPk];
export type PRODUTOSOptionalAttributes = "id";
export type PRODUTOSCreationAttributes = Optional<
	PRODUTOSAttributes,
	PRODUTOSOptionalAttributes
>;

export class PRODUTOS
	extends Model<PRODUTOSAttributes, PRODUTOSCreationAttributes>
	implements PRODUTOSAttributes
{
	id!: number;
	nome!: string;
	preco!: number;

	// PRODUTOS belongsToMany VENDAS via id_produto and id_venda
	id_venda_vendas!: VENDAS[];
	getId_venda_vendas!: Sequelize.BelongsToManyGetAssociationsMixin<VENDAS>;
	setId_venda_vendas!: Sequelize.BelongsToManySetAssociationsMixin<
		VENDAS,
		VENDASId
	>;
	addId_venda_venda!: Sequelize.BelongsToManyAddAssociationMixin<
		VENDAS,
		VENDASId
	>;
	addId_venda_vendas!: Sequelize.BelongsToManyAddAssociationsMixin<
		VENDAS,
		VENDASId
	>;
	createId_venda_venda!: Sequelize.BelongsToManyCreateAssociationMixin<VENDAS>;
	removeId_venda_venda!: Sequelize.BelongsToManyRemoveAssociationMixin<
		VENDAS,
		VENDASId
	>;
	removeId_venda_vendas!: Sequelize.BelongsToManyRemoveAssociationsMixin<
		VENDAS,
		VENDASId
	>;
	hasId_venda_venda!: Sequelize.BelongsToManyHasAssociationMixin<
		VENDAS,
		VENDASId
	>;
	hasId_venda_vendas!: Sequelize.BelongsToManyHasAssociationsMixin<
		VENDAS,
		VENDASId
	>;
	countId_venda_vendas!: Sequelize.BelongsToManyCountAssociationsMixin;
	// PRODUTOS hasMany VENDAS_PRODUTOS via id_produto
	vendas_produtos!: VENDAS_PRODUTOS[];
	getVendas_produtos!: Sequelize.HasManyGetAssociationsMixin<VENDAS_PRODUTOS>;
	setVendas_produtos!: Sequelize.HasManySetAssociationsMixin<
		VENDAS_PRODUTOS,
		VENDAS_PRODUTOSId
	>;
	addVendas_produto!: Sequelize.HasManyAddAssociationMixin<
		VENDAS_PRODUTOS,
		VENDAS_PRODUTOSId
	>;
	addVendas_produtos!: Sequelize.HasManyAddAssociationsMixin<
		VENDAS_PRODUTOS,
		VENDAS_PRODUTOSId
	>;
	createVendas_produto!: Sequelize.HasManyCreateAssociationMixin<VENDAS_PRODUTOS>;
	removeVendas_produto!: Sequelize.HasManyRemoveAssociationMixin<
		VENDAS_PRODUTOS,
		VENDAS_PRODUTOSId
	>;
	removeVendas_produtos!: Sequelize.HasManyRemoveAssociationsMixin<
		VENDAS_PRODUTOS,
		VENDAS_PRODUTOSId
	>;
	hasVendas_produto!: Sequelize.HasManyHasAssociationMixin<
		VENDAS_PRODUTOS,
		VENDAS_PRODUTOSId
	>;
	hasVendas_produtos!: Sequelize.HasManyHasAssociationsMixin<
		VENDAS_PRODUTOS,
		VENDAS_PRODUTOSId
	>;
	countVendas_produtos!: Sequelize.HasManyCountAssociationsMixin;

	static initModel(sequelize: Sequelize.Sequelize): typeof PRODUTOS {
		return PRODUTOS.init(
			{
				id: {
					autoIncrement: true,
					type: DataTypes.INTEGER,
					allowNull: false,
					primaryKey: true,
				},
				nome: {
					type: DataTypes.STRING(45),
					allowNull: false,
				},
				preco: {
					type: DataTypes.DECIMAL(10, 2),
					allowNull: false,
				},
			},
			{
				sequelize,
				tableName: "produtos",
				timestamps: false,
				indexes: [
					{
						name: "PRIMARY",
						unique: true,
						using: "BTREE",
						fields: [{ name: "id" }],
					},
				],
			}
		);
	}
}

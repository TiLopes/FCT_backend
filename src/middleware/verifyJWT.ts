import jwt, { VerifyErrors } from "jsonwebtoken";
import { initModels } from "../models/init-models";
import db from "../models";
import { RequestHandler } from "express";
import logger from "../utils/logger";

const { USER } = initModels(db);

const verifyJWT: RequestHandler = (req, res, next) => {
	const token = req.headers.authorization?.split(" ")[1];

	if (!token) {
		return res.status(401).json({ error: "No token" });
	}

	logger.info({ token });

	jwt.verify(
		token,
		process.env.ACCESS_TOKEN_SECRET as string,
		async (err: VerifyErrors | null, decodedToken: unknown) => {
			if (err) {
				logger.error(err.message);
				return res.status(403).json({ error: "Token is invalid" });
			}
			logger.info(token);
			const user = await USER.findOne({ where: { authToken: token } });
			if (!user) {
				return res.status(401).json({ error: "Invalid user token" });
			}

			req.userID = user.id;

			next();
		}
	);
};

export default verifyJWT;

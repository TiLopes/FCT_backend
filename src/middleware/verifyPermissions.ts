import { RequestHandler } from "express";
import db from "../models";
import { initModels } from "../models/init-models";
import logger from "../utils/logger";

const { USER, GROUP_PERMISSIONS } = initModels(db);

const verifyPermission: RequestHandler = async (req, res, next) => {
	const token = req.headers.authorization?.split(" ")[1];

	if (!token) {
		return res.status(403).json({ error: "Invalid token" });
	}

	logger.info({ token });

	const user = await USER.findOne({ where: { authToken: token } });

	if (!user) {
		return res.status(403).json({ error: "Invalid user token" });
	}

	const acl = await GROUP_PERMISSIONS.findAll({
		where: { group_id: user.group_id },
		attributes: ["permission_id"],
	}).then((mapACL) => {
		let x = mapACL.map((perm) => perm.permission_id);
		return x;
	});

	logger.info({ acl });

	const url = new URL(req.protocol + "://" + req.get("host") + req.originalUrl);

	const pathSplit = url.pathname.split("/");
	logger.info(pathSplit);
	const action = pathSplit[2];
	const spec = pathSplit[3];
	const fullPerm = action + ":" + spec;
	logger.info(fullPerm);

	if (acl.includes(fullPerm.toLowerCase()) || acl.includes("*:*")) {
		next();
	} else {
		return res.status(401).json({ error: "Invalid permission" });
	}
};

export default verifyPermission;

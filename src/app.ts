import cors from "cors";
import dotenv from "dotenv";
import express from "express";
import authRoutes from "./routes/auth.routes";
import clientesRoutes from "./routes/clientes.routes";
import faltasRoutes from "./routes/faltas.routes";
import produtosRoutes from "./routes/produtos.routes";
import protectedRoutes from "./routes/protected.routes";
import userRoutes from "./routes/users.routes";
import vendasRoutes from "./routes/vendas.routes";

dotenv.config();

const app = express();
const PORT = 3000;

app.use(cors());
app.use(express.json());
app.use(express.static(__dirname + "/public"));

app.use(authRoutes);
app.use(protectedRoutes);
app.use("/api", userRoutes);
app.use("/api", clientesRoutes);
app.use("/api", produtosRoutes);
app.use("/api", vendasRoutes);
app.use("/api", faltasRoutes);

app.listen(PORT, () => {
	console.log(`Server started on port: ${PORT}`);
});

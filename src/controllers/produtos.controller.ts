import { RequestHandler, Response } from "express";
import db from "../models";
import { PRODUTOSAttributes, initModels } from "../models/init-models";
import logger from "../utils/logger";

const { PRODUTOS } = initModels(db);

const getProdutos: RequestHandler = async (req, res) => {
	let page = 0;
	let per_page = 10;
	let sort = "id";
	let order = "asc";

	if (Object.keys(req.query).length === 0) {
		getAll(res);
		return;
	}

	if (req.query.page) page = Number(req.query.page) - 1;

	if (req.query.per_page) per_page = Number(req.query.per_page);

	if (req.query.sort) sort = String(req.query.sort);

	if (req.query.order) order = String(req.query.order);

	try {
		const total = await PRODUTOS.count();
		const produtos = await PRODUTOS.findAll({
			offset: page * per_page,
			limit: per_page,
			order: [[sort, order]],
			nest: true,
		});

		logger.info({ produtos });

		res.status(200).json({
			page: page + 1,
			per_page,
			current_page: page + 1,
			total,
			total_pages: Math.floor(total / per_page),
			data: produtos,
		});
	} catch (e) {
		logger.error(e);
		res.status(400).json(e);
	}
};

const getAll = async (res: Response) => {
	try {
		const produtos = await PRODUTOS.findAll();
		res.status(200).json({
			produtos,
		});
	} catch (err) {
		logger.error(err);
		res.status(400).json(err);
	}
};

const addProduto: RequestHandler = async (req, res) => {
	const {
		nome,
		preco,
	}: {
		nome: string;
		preco: number;
	} = req.body;

	try {
		await PRODUTOS.create({
			nome,
			preco,
		});

		res.status(200).json({ success: true });
	} catch (err) {
		logger.error({ err });
		res.status(400).json({ success: false, err });
	}
};

const getProduto: RequestHandler = async (req, res) => {
	const { id } = req.params;

	try {
		const produto = await PRODUTOS.findByPk(id);

		if (!produto) throw Error("Produto não existe");

		res.status(200).json({ success: true, produto });
	} catch (err) {
		logger.error({ err });
		res.status(400).json({ success: false, err });
	}
};

const updateProduto: RequestHandler = async (req, res) => {
	const produto: PRODUTOSAttributes = req.body;
	logger.info({ produto });
	const { id: id_produto } = req.params;

	try {
		// throw new Error("asdasad");
		await PRODUTOS.update(
			{
				nome: produto.nome,
				preco: produto.preco,
			},
			{
				where: {
					id: id_produto,
				},
			}
		);
		res.status(200).json({ success: true });
	} catch (err) {
		logger.error({ err });
		res.status(400).json({ success: false, err });
	}
};

const deleteProduto: RequestHandler = async (req, res) => {
	const { id: id_produto } = req.params;

	try {
		await PRODUTOS.destroy({
			where: {
				id: id_produto,
			},
		});
		res.status(200).json({ success: true });
	} catch (err) {
		logger.error({ err });
		res.status(400).json({ success: false, err });
	}
};

export { getProdutos, getProduto, addProduto, updateProduto, deleteProduto };

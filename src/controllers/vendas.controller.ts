import { RequestHandler } from "express";
import db from "../models";
import { initModels } from "../models/init-models";
import logger from "../utils/logger";

const { VENDAS, VENDAS_PRODUTOS, CLIENTES, PRODUTOS } = initModels(db);

const getVendas: RequestHandler = async (req, res) => {
	let page = 0;
	let per_page = 10;
	let sort = "id";
	let order = "asc";
	let vendas: unknown[] = [];

	if (req.query.page) page = Number(req.query.page) - 1;

	if (req.query.per_page) per_page = Number(req.query.per_page);

	if (req.query.sort) sort = String(req.query.sort);

	if (req.query.order) order = String(req.query.order);

	logger.info({ order });

	try {
		const total = await VENDAS.count();
		const vendasRAW = await VENDAS.findAll({
			offset: page * per_page,
			limit: per_page,
			order: [[sort, order]],
			include: [
				{
					model: VENDAS_PRODUTOS,
					as: "vendas_produtos",
					include: [
						{
							model: PRODUTOS,
							as: "id_produto_produto",
						},
					],
				},
				{
					model: CLIENTES,
					as: "id_cliente_cliente",
				},
			],
			nest: true,
		});
		let produtos: any[] = [];

		for (const venda of vendasRAW) {
			for (const venda_produto of venda.vendas_produtos) {
				produtos.push({
					...venda_produto.id_produto_produto.dataValues,
					quantidade: venda_produto.quantidade,
				});
			}
			vendas.push({
				id: venda.id,
				data_venda: venda.data_venda,
				preco: venda.preco,
				produtos,
				cliente: venda.id_cliente_cliente,
				// venda_produtos: venda.vendas_produtos,
			});
			produtos = [];
		}

		logger.info({ vendas });

		res.status(200).json({
			page: page + 1,
			per_page,
			current_page: page + 1,
			total,
			total_pages: Math.floor(total / per_page),
			data: vendas,
		});
	} catch (e) {
		logger.error(e);
		res.status(400).json(e);
	}
};

const addVenda: RequestHandler = async (req, res) => {
	const {
		cliente,
		data_venda,
		preco,
		produtos,
	}: {
		cliente: number;
		data_venda: Date;
		preco: number;
		produtos: { id: number; quantidade: number }[];
	} = req.body;

	try {
		const venda = await VENDAS.create({
			id_cliente: cliente,
			data_venda,
			preco,
		});

		for (const produto of produtos) {
			await VENDAS_PRODUTOS.create({
				id_produto: produto.id,
				id_venda: venda.id,
				quantidade: produto.quantidade,
			});
		}

		res.status(200).json({ success: true });
	} catch (err) {
		logger.error(err);
		res.status(400).json({ success: false, err });
	}
};

// const addVenda: RequestHandler = async (req, res) => {
// 	const {
// 		nome,
// 		preco,
// 	}: {
// 		nome: string;
// 		preco: number;
// 	} = req.body;

// 	try {
// 		await VENDAS.create({
// 			nome,
// 			preco,
// 		});

// 		res.status(200).json({ success: true });
// 	} catch (err) {
// 		logger.error({ err });
// 		res.status(400).json({ success: false, err });
// 	}
// };

const getVenda: RequestHandler = async (req, res) => {
	const { id } = req.params;

	try {
		const venda = await VENDAS.findByPk(id);

		if (!venda) throw Error("Venda não existe");

		res.status(200).json({ success: true, venda });
	} catch (err) {
		logger.error({ err });
		res.status(400).json({ success: false, err });
	}
};

// const updateVenda: RequestHandler = async (req, res) => {
// 	const venda: VENDASAttributes = req.body;
// 	logger.info({ venda });
// 	const { id: id_venda } = req.params;

// 	try {
// 		// throw new Error("asdasad");
// 		await VENDAS.update(
// 			{
// 				nome: venda.nome,
// 				preco: venda.preco,
// 			},
// 			{
// 				where: {
// 					id: id_venda,
// 				},
// 			}
// 		);
// 		res.status(200).json({ success: true });
// 	} catch (err) {
// 		logger.error({ err });
// 		res.status(400).json({ success: false, err });
// 	}
// };

const deleteVenda: RequestHandler = async (req, res) => {
	const { id: id_venda } = req.params;

	try {
		await VENDAS.destroy({
			where: {
				id: id_venda,
			},
		});
		res.status(200).json({ success: true });
	} catch (err) {
		logger.error({ err });
		res.status(400).json({ success: false, err });
	}
};

export { getVendas, getVenda, addVenda, /*updateVenda,*/ deleteVenda };

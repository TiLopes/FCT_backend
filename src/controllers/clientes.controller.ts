import { RequestHandler, Response } from "express";
import db from "../models";
import { CLIENTESAttributes, initModels } from "../models/init-models";
import logger from "../utils/logger";

const { CLIENTES } = initModels(db);

const getClientes: RequestHandler = async (req, res) => {
	let page = 0;
	let per_page = 10;
	let sort = "id";
	let order = "asc";
	const { type } = req.params;

	if (Object.keys(req.query).length === 0) {
		if (type === "basic") return getAllBasic(res);
		return getAllExtensive(res);
	}

	if (req.query.page) page = Number(req.query.page) - 1;

	if (req.query.per_page) per_page = Number(req.query.per_page);

	if (req.query.sort) sort = String(req.query.sort);

	logger.info({ sort });

	if (req.query.order) order = String(req.query.order);

	logger.info({ order });

	try {
		const total = await CLIENTES.count();
		const clientes = await CLIENTES.findAll({
			offset: page * per_page,
			limit: per_page,
			order: [[sort, order]],
			nest: true,
		});

		logger.info({ clientes });

		res.status(200).json({
			page: page + 1,
			per_page,
			current_page: page + 1,
			total,
			total_pages: Math.floor(total / per_page),
			data: clientes,
		});
	} catch (e) {
		logger.error(e);
		res.status(400).json(e);
	}
};

const getAllBasic = async (res: Response) => {
	try {
		const clientes = await CLIENTES.findAll({
			attributes: ["id", "nome"],
		});

		res.status(200).json({ clientes });
	} catch (err) {
		logger.error(err);
		res.status(400).json(err);
	}
};

const getAllExtensive = async (res: Response) => {
	try {
		const clientes = await CLIENTES.findAll();

		res.status(200).json({ clientes });
	} catch (err) {
		logger.error(err);
		res.status(400).json(err);
	}
};

const addCliente: RequestHandler = async (req, res) => {
	const {
		email,
		nome,
		nif,
		telefone,
		cod_postal,
		morada,
	}: {
		email: string;
		nome: string;
		nif: number;
		telefone: string;
		cod_postal: string;
		morada: string;
	} = req.body;

	try {
		await CLIENTES.create({
			email,
			nome,
			nif,
			telefone,
			cod_postal,
			morada,
		});

		res.status(200).json({ success: true });
	} catch (err) {
		logger.error({ err });
		res.status(400).json({ success: false, err });
	}
};

const getCliente: RequestHandler = async (req, res) => {
	const { id } = req.params;

	try {
		const cliente = await CLIENTES.findByPk(id);

		if (!cliente) throw Error("Cliente não existe");

		res.status(200).json({ success: true, cliente });
	} catch (err) {
		logger.error({ err });
		res.status(400).json({ success: false, err });
	}
};

const updateCliente: RequestHandler = async (req, res) => {
	const cliente: CLIENTESAttributes = req.body;
	logger.info({ cliente });
	const { id: id_cliente } = req.params;

	try {
		// throw new Error("asdasad");
		await CLIENTES.update(
			{
				nome: cliente.nome,
				email: cliente.email,
				telefone: cliente.telefone,
				nif: cliente.nif,
				cod_postal: cliente.cod_postal,
				morada: cliente.morada,
			},
			{
				where: {
					id: id_cliente,
				},
			}
		);
		res.status(200).json({ success: true });
	} catch (err) {
		logger.error({ err });
		res.status(400).json({ success: false, err });
	}
};

export { getClientes, addCliente, getCliente, updateCliente };

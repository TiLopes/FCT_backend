import { RequestHandler } from "express";
import { Op, WhereOptions } from "sequelize";
import db from "../models";
import { FALTASAttributes, initModels } from "../models/init-models";
import logger from "../utils/logger";

const { FALTAS, USER } = initModels(db);

const getFaltas: RequestHandler = async (req, res) => {
	const { data_inicio, data_fim } = req.query;
	try {
		const faltasRAW = await FALTAS.findAll({
			where: {
				data_inicio: {
					[Op.gte]: data_inicio,
				},
				data_fim: {
					[Op.lt]: data_fim,
				},
			} as WhereOptions<FALTASAttributes>,
			include: {
				model: USER,
				as: "id_utilizador_user",
			},
		});

		let faltas: any[] = [];
		for (const falta of faltasRAW) {
			faltas.push({
				id: falta.id,
				user: falta.id_utilizador_user.nome,
				motivo: falta.motivo,
				data_fim: falta.data_fim,
				data_inicio: falta.data_inicio,
				dia_completo: falta.dia_completo,
				estado: falta.estado,
			});
		}

		res.status(200).json({ success: true, faltas });
	} catch (err) {
		logger.error({ err });
		res.status(400).json({ success: false, err });
	}
};

const justificarFalta: RequestHandler = async (req, res) => {
	const { id }: { id: number } = req.body;

	try {
		await FALTAS.update(
			{
				estado: "Justificada",
			},
			{ where: { id } }
		);
		res.status(200).json({ success: true });
	} catch (err) {
		logger.error({ err });
		res.status(400).json({ success: false, err });
	}
};

const addFalta: RequestHandler = async (req, res) => {
	const {
		id_utilizador,
		motivo,
		data_inicio,
		data_fim,
		dia_completo,
	}: {
		id_utilizador: number;
		motivo: string;
		data_inicio: Date;
		data_fim: Date;
		dia_completo: boolean;
	} = req.body;

	await FALTAS.create({
		id_utilizador,
		motivo,
		data_inicio,
		data_fim,
		dia_completo,
	});

	res.status(200).json({ success: true });
};

export { getFaltas, justificarFalta, addFalta };

import { initModels } from "../models/init-models";
import db from "../models";
import { RequestHandler } from "express";

const { USER } = initModels(db);

const userInfoGET: RequestHandler = async (req, res) => {
	const user = await USER.findByPk(req.params.userId, {
		attributes: { exclude: ["password", "authToken"] },
	});

	if (!user) {
		return res.status(404).json({ error: "User does not exist" });
	}

	res.status(200).json({ user });
};

const personalInfoGET: RequestHandler = async (req, res) => {
	const user = await USER.findByPk(req.userID, {
		attributes: {
			exclude: ["password", "authToken"],
		},
	});

	if (!user) {
		return res.status(404).json({ error: "User does not exist" });
	}

	res.status(200).json({
		user: {
			id: user.id,
			group_id: user.group_id,
			email: user.email,
			createdAt: user.createdAt,
			updatedAt: user.updatedAt,
		},
	});
};

export { userInfoGET, personalInfoGET };

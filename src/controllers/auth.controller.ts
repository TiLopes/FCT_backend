import bcrypt from "bcrypt";
import { RequestHandler } from "express";
import jwt, { JwtPayload } from "jsonwebtoken";
import crypto from "node:crypto";
import { Op } from "sequelize";
import db from "../models";
import { initModels } from "../models/init-models";
import logger from "../utils/logger";
import resetPasswordEmail from "../utils/resetPasswordEmail";
// dotenv.config();

const { USER } = initModels(db);

const handleErrors = (err) => {
	let errs = { email: "", password: "" };

	logger.info(err.message);

	// SIGN UP
	if (err.message.includes("Validation error")) {
		logger.info(err.errors[0].message);
		if (err.errors[0].message === "Email vazio") {
			errs.email = "Introduza um email";
			return errs;
		}

		if (err.errors[0].message === "Password vazia") {
			errs.password = "Introduza uma password";
			return errs;
		}

		if (err.errors[0].message === "Introduza no minimo 5 caracteres") {
			errs.password = "Escreva no mínimo 5 caracteres";
			return errs;
		}

		if (err.errors[0].message === "Email duplicado") {
			errs.email = "Esse email já foi registado";
			return errs;
		}
	}

	// LOG IN
	if (err.message === "Email vazio") {
		errs.email = "Introduza um email";
		return errs;
	}

	if (err.message === "Password vazia") {
		errs.password = "Introduza uma password";
		return errs;
	}

	if (err.message === "Esse email não está registado") {
		errs.email = "Esse email não está registado";
		return errs;
	}

	if (err.message === "Password errada") {
		errs.password = "A password está incorreta";
		return errs;
	}

	return errs;
};

// SIGN UP
const signupGET = (req, res) => {
	res.status(200).json({ success: true });
};

const signupPOST: RequestHandler = async (req, res) => {
	const {
		email,
		password,
		nif,
		nome,
		cod_postal,
		morada,
	}: {
		email: string;
		password: string;
		nif: number;
		nome: string;
		cod_postal: string;
		morada: string;
	} = req.body;

	try {
		const user = await USER.create({
			email,
			password,
			nif,
			nome,
			cod_postal,
			morada,
		});
		res.status(200).json({
			user: {
				group_id: user.group_id,
				id: user.id,
				email: user.email,
			},
		});
	} catch (err) {
		const errors = handleErrors(err);
		res.status(400).json({ errors });
	}
};

// LOGIN
const loginGET: RequestHandler = (req, res) => {
	res.status(200).json({ success: true });
};

const loginPOST: RequestHandler = async (req, res) => {
	const { email, password } = req.body;

	try {
		const user = await USER.login(email, password);

		// tokens creation
		const accessToken = jwt.sign(
			{ id: user.id, group_id: user.group_id },
			process.env.ACCESS_TOKEN_SECRET as string,
			{ expiresIn: "8h" }
		);

		await USER.saveToken(user.id, accessToken);

		const accDate = jwt.decode(accessToken) as JwtPayload;

		res.status(200).json({
			user: {
				id: user.id,
				group_id: user.group_id,
				email: user.email,
				accessToken: {
					token: accessToken,
					expires: accDate.exp,
				},
			},
		});
	} catch (err) {
		const errs = handleErrors(err);
		logger.error(errs);
		res.status(400).json(errs);
	}
};

// LOG OUT
// module.exports.logout_get = async (req, res) => {
//   // Cookies
//   const cookies = req.cookies;

//   if (!cookies?.refreshToken) {
//     return res.status(403).json({ clearCookies: false, redirect: true });
//   }

//   // Encontrar refreshToken na db
//   const refreshToken = cookies.refreshToken;
//   const user = await User.findOne({ where: { authToken: refreshToken } });

//   if (!user) {
//     return res.status(403).json({ clearCookies: true, redirect: true });
//   }

//   // Remover refreshToken da db
//   await User.removeToken(user);

//   res.status(200).json({ clearCookies: true, redirect: true });
// };

const resetPasswordRequest: RequestHandler = async (req, res) => {
	const { email } = req.body;

	const user = await USER.findOne({
		where: {
			email,
		},
	});

	if (!user) {
		return res.status(400).json({ err: "Utilizador não existe" });
	}

	logger.info({ user: user?.dataValues });
	let resetToken = crypto.randomBytes(32).toString("hex");
	const salt = await bcrypt.genSalt();
	const hashBcrypt = await bcrypt.hash(resetToken + Date.now(), salt);
	const hashClient = Buffer.from(hashBcrypt).toString("base64url");
	logger.info({ hashBcrypt });
	const timeLimit = new Date(Date.now() + 2 * 3600 * 1000)
		.toISOString()
		.slice(0, 19)
		.replace("T", " ");

	await USER.update(
		{
			recover_password_token: hashBcrypt,
			recover_password_token_date: timeLimit,
		},
		{
			where: {
				id: user.id,
			},
		}
	);
	logger.info(Buffer.from(hashClient, "base64url").toString("utf8"));

	resetPasswordEmail(user, hashClient);

	res.sendStatus(200);
};

const resetPasswordTokenValid: RequestHandler = async (req, res) => {
	const { token } = req.params;

	const user = await USER.findOne({
		where: {
			recover_password_token: Buffer.from(token, "base64url").toString("utf8"),
			recover_password_token_date: {
				[Op.gte]: new Date().toISOString().slice(0, 19).replace("T", " "),
			},
		},
	});

	if (!user) {
		return res.status(400).json({ err: "Utilizador não existe" });
	}

	res.status(200).json({ exists: true });
};

const resetPassword: RequestHandler = async (req, res) => {
	const { password, token } = req.body;

	const user = await USER.findOne({
		where: {
			recover_password_token: Buffer.from(token, "base64url").toString("utf8"),
		},
	});

	if (!user) {
		return res.status(400).json({ err: "Utilizador não existe" });
	}

	const salt = await bcrypt.genSalt();

	await USER.update(
		{
			password: bcrypt.hashSync(password, salt),
			recover_password_token: "",
		},
		{
			where: {
				id: user.id,
			},
		}
	);

	res.status(200).json({ success: true });
};

export {
	loginGET,
	loginPOST,
	signupGET,
	signupPOST,
	resetPasswordRequest,
	resetPassword,
	resetPasswordTokenValid,
};

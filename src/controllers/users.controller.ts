import { RequestHandler, Response } from "express";
import db from "../models";
import { initModels } from "../models/init-models";
import logger from "../utils/logger";

const { USER, GROUPS } = initModels(db);

const getUsers: RequestHandler = async (req, res) => {
	let page = 0;
	let per_page = 10;

	const { type } = req.params;

	if (Object.keys(req.query).length === 0) {
		if (type === "basic") return getAllBasic(res);

		return getAllExtensive(res);
	}

	if (req.query.page) page = Number(req.query.page) - 1;

	if (req.query.per_page) per_page = Number(req.query.per_page);

	logger.info({ per_page });

	try {
		const total = await USER.count();
		const users = await USER.findAll({
			offset: page * per_page,
			limit: per_page,
			attributes: {
				exclude: [
					"authToken",
					"password",
					"morada",
					"cod_postal",
					"recover_password_token",
					"recover_password_token_date",
					"group_id",
				],
			},
			include: {
				model: GROUPS,
				as: "group",
			},
			nest: true,
		});

		logger.info({ users });

		res.status(200).json({
			page: page + 1,
			per_page,
			current_page: page + 1,
			total,
			total_pages: Math.floor(total / per_page),
			data: users,
		});
	} catch (e) {
		logger.error(e);
		res.status(400).json(e);
	}
};

const getAllBasic = async (res: Response) => {
	try {
		const users = await USER.findAll({
			attributes: ["id", "nome"],
		});

		res.status(200).json({ sucess: true, users });
	} catch (err) {
		logger.error(err);
		res.status(400).json({ success: false, err });
	}
};

const getAllExtensive = async (res: Response) => {
	try {
		const users = await USER.findAll({
			attributes: {
				exclude: [
					"authToken",
					"password",
					"morada",
					"cod_postal",
					"recover_password_token",
					"recover_password_token_date",
					"group_id",
				],
			},
			include: {
				model: GROUPS,
				as: "group",
			},
			nest: true,
		});

		res.status(200).json({ sucess: true, users });
	} catch (err) {
		logger.error(err);
		res.status(400).json({ success: false, err });
	}
};

export { getUsers };
